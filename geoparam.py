
import numpy as np

#Known variables

pa = 1e5    #mean pressure (Pa)
pb = 200.   #magnitude of pressure variations (Pa)
f = 1e-4    #Coriolis parameter
rho = 1.    #density
L = 2.4e6   #length scale of pressure variations (k)
ymin = 0.0  # minimum space dimention
ymax = 1e6  # max space dimention

def pres(y):
    "formula for finding the pressure at a location y"
    return pa + pb*np.cos(y*np.pi/L)

def uexact(y):
    "analytic solution for the geostrophic wind at location y"

    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geowind(dpdy):
    "geostrophic wind as a function of the pressure gradient"
    
    return -dpdy/(rho*f)
