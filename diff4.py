
import numpy as np

#function to differentiate a given input

def diff4(f, dx):
    
    dfdx = np.zeros_like(f)
    
    #start points
    dfdx[0] = (4*f[1] - 3*f[0] - f[2])/(2*dx)             #3 point forward => 2nd order
    dfdx[1] = (6*f[2] - f[3] - 3*f[1] - 2*f[0])/(6*dx)      #4 point forward => 3rd order
    
    #end points
    dfdx[-1] = (3*f[-1] - 4*f[-2] + f[-3])/(2*dx)          #3 point backward => 2nd order
    dfdx[-2] = (2*f[-1] + 3*f[-2] - 6*f[-3] + f[-4])/(6*dx)  #4 point backward => 3rd order

    
    for i in range(2, len(f)-2):

        dfdx[i] = (8*f[i+1] - 8*f[i-1] - f[i+2] + f[i-2])/(12*dx)
        
    return dfdx
