
import numpy as np
import matplotlib.pyplot as plt

from diff4 import *
from diff2 import *



def impgeostwind():
    
    "Numerically differentiate pressure function to calculate the\
    geostrophic wind relation using 2 point differences, compare and\
    plot against the analytic solution, and plots log graph of errors"
    
    import geoparam as gp
    
    #set resolution:
    
    #N is the number of intervals the space will be divided into
    for N in range(4,30,5):
        
        #check that N and ymax are not too small
        #if N too small: dividing by zero
        
        if abs(N) < 1e-6:
            break
            
        #if ymax too close to ymin, dy will end up being zero
        
        if abs(gp.ymax) < abs(gp.ymin):
            break
            
    #dy is the step size

        dy = (gp.ymax - gp.ymin)/N
        
        #y is the spatial dimention

        y = np.linspace(gp.ymin, gp.ymax, N+1)
        
        #uexact (pressure gradient) is the analytical solution for 
        #geostrophic wind

        uexact = gp.uexact(y)
        
        #pressure at each point y(i)
        
        p = gp.pres(y)
        
        #the calculated numerical pressure gradient and wind 

        dpdy = diff4(p, dy)
        dpdy2pt = diff2(p, dy)
        
        ucentre = gp.geowind(dpdy)
        
        ucentre2pt = gp.geowind(dpdy2pt)

        uerror = abs(uexact - ucentre)
        
        uerror2pt = abs(uexact - ucentre2pt)
        
        
        #graph of numerical vs analytic solutions

        fg1 = plt.figure(figsize=[6,4])
        font = {'size': 12}
        plt.rc('font', **font)
        plt.plot(y/1000, ucentre, '*r--', label='two-point differences',
                 ms=12, markeredgewidth=1.5, markerfacecolor='none')
        plt.plot(y/1000, uexact, 'k-', label='Exact')
        plt.title(r'differentiating $u$ $=$ $\frac{1}{\rho f}$ $\frac{dp}{dy}$',
                  y=1.01)
        plt.legend(loc='best')
        plt.xlabel('y km')
        plt.ylabel('u m/s')
        plt.tight_layout()
        plt.savefig('Plots/impgeowindcent.pdf')
        plt.show()
        
        #logarithmic plot of error between analytic and numerical
        #of both errors from 2nd and 4th orders

        fg3 = plt.figure(figsize=[6,4])
        font = {'size': 12}
        plt.rc('font', **font)
        plt.loglog(y/1000, uerror, 'b-', label='4th order error')
        plt.loglog(y/1000, uerror2pt, 'r-', label='2nd order error')
        plt.title('Difference in error between 2nd and 4th order approx',y=1.01)
        plt.legend(loc='best')
        plt.xlabel('y km')
        plt.ylabel('absolute error km')
        plt.ylim((10e-9,1))  #to compare all outputs on same scale
        plt.tight_layout()
        plt.savefig('Plots/impgeowindcenterr.pdf')
        plt.show()        
        
    
if __name__ == "__main__":
    impgeostwind()
