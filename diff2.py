
import numpy as np

#function to differentiate a given input

def centrediff(f, dx):
    "Numerically differentiating the function f, with points\
    a distance dx apart, and using a 2 point taylor approximated\
    difference"
    
    #initalising the output's array of the gradient to be
    #the same size as the array of the input function f
    
    dfdx = np.zeros_like(f)
    
    #2 point forward difference method at start point => first order
    dfdx[0] = (f[1] - f[0])/dx
    
    #2 point backward difference method at end point => first order
    dfdx[-1] = (f[-1] - f[-2])/dx
    
    #2 point centred difference for rest of points => second order
    
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
        
    return dfdx
