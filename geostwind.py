
import numpy as np
import matplotlib.pyplot as plt

from diff2 import *

def geostwind():
    
    "Numerically differentiate pressure function to calculate the\
    geostrophic wind relation using 2 point differences, compare and\
    plot against the analytic solution, and plots log graph of errors"
    
    import geoparam as gp
    
    #set resolution:
    
    #N is the number of intervals the space will be divided into
    
    #Note this code runs over a range of values for N (from 5 to 30), 
    #and the required N = 10 for this assignment is the 2nd output plot
    
    
    ###check dy behaves as expected using small ymax and N
        
    for Nc in range(1, 4):
        for ymaxc in range(1, 5):
        
            dyc = (ymaxc - gp.ymin)/Nc
            print('at N =', Nc, 'and ymax =', ymaxc, 'checked dy =', dyc)
    
    for N in range(4,30,5):
        
        #check that N and ymax are not too small
        #if N too small: dividing by zero
        
        if abs(N) < 1e-6:
            break
            
        #if ymax too close to ymin, dy will end up being zero
        
        if abs(gp.ymax) < abs(gp.ymin):
            break
            
    #dy is the step size

        dy = (gp.ymax - gp.ymin)/N
        
        
        #y is the spatial dimention

        y = np.linspace(gp.ymin, gp.ymax, N+1)
        
        #uexact (pressure gradient) is the analytical solution for 
        #geostrophic wind

        uexact = gp.uexact(y)
        
        #pressure at each point y(i)
        
        p = gp.pres(y)
        
        #the calculated numerical pressure gradient and wind 

        dpdy = diff2(p, dy)
        
        ucentre = gp.geowind(dpdy)

        uerror = abs(uexact - ucentre)
        
        
        #graph of numerical vs analytic solutions

        fg1 = plt.figure(figsize=[6,4])
        font = {'size': 12}
        plt.rc('font', **font)
        plt.plot(y/1000, ucentre, '*r--', label='two-point differences',
                 ms=12, markeredgewidth=1.5, markerfacecolor='none')
        plt.plot(y/1000, uexact, 'k-', label='Exact')
        plt.title(r'differentiating $u$ $=$ $\frac{1}{\rho f}$ $\frac{dp}{dy}$',
                  y=1.01)
        plt.legend(loc='best')
        plt.xlabel('y km')
        plt.ylabel('u m/s')
        plt.tight_layout()
        plt.savefig('Plots/geowindcent.pdf')
        plt.show()
        
        #logarithmic plot of error between analytic and numerical

        fg2 = plt.figure(figsize=[6,4])
        font = {'size': 12}
        plt.rc('font', **font)
        plt.loglog(y/1000, uerror, 'b-', label='Error')
        plt.title(r'Error from numerically differentiating $u$ $=$ $\frac{1}{\rho f}$ $\frac{dp}{dy}$',y=1.01)
        plt.legend(loc='best')
        plt.xlabel('y km')
        plt.ylabel('absolute error km')
        plt.ylim((10e-6,1))  #to compare all outputs on same scale
        plt.tight_layout()
        plt.savefig('Plots/geowindcenterr.pdf')
        plt.show()
    
if __name__ == "__main__":
    geostwind()
